var webpack = require('webpack');
var path = require('path');
var vue = require('vue-loader');
var _ = require('underscore');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var HtmlWebpackPlugin = require('html-webpack-plugin')

var resolveBowerPath = function(pluginName) {
    return path.join(__dirname, 'bower_components', pluginName, 'dist', pluginName, '.js');
};

var resolveNodePath = function(pluginName) {
    return path.join(__dirname, 'node_modules', pluginName, 'dist', pluginName);
};

//拼装query
var objToQuery = function(obj) {
  if (_.isObject(obj)) {
    var str = _.map(obj, function(value, key) {
      return [key, '=', value].join('');
    });
    return str.join("&");
  }
  return '';
}
//优雅地书写loader附加参数
//loaders(['style', 'css', 'autoprefixer', {name: 'sass', query: {outputStyle: 'compressed'}}])
var loaders = function(loaders, isArray) {
  if (_.isArray(loaders)) {
    var loaderArray = _.map(loaders, function(loader) {
      return _.isString(loader) ? loader : [loader.name, '?', objToQuery(loader.query)].join('');
    });
    return !isArray ? loaderArray.join('!'): loaderArray;
  }
  return '';
}


module.exports = {
  
  entry: {
    entry: ['./src/entry'],
    vendor: ['vue', 'vue-router'],
  },
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, './production/'),
  },
  module: {
    loaders: [{ 
      test: /\.scss$/, 
      loaders: loaders(['style', 'css', 'autoprefixer', {name: 'sass', query: {outputStyle: 'compressed'}}], true)
    }, {
      test: /\.vue$/, 
      loader: vue.withLoaders({
          js: 'babel?optional[]=runtime',
          css: ExtractTextPlugin.extract("css"),
          sass: loaders(['style', 'css', 'autoprefixer', {name: 'sass', query: {outputStyle: 'compressed'}}])
        })
    }, {
      test: /\.js$/,
      exclude: /(node_modules|bower_components)/,
      loader: 'babel?optional[]=runtime&stage=0'
    }, {
      test: /\.(png|jpg)\?__inline$/,
      loader: 'url?limit=25000'
    }, {
      test: /\.(png|jpg)$/,
      //使用hash值，避免YY DNS上资源缓存问题
      loader: 'file?name=images/[name]-[sha512:hash:base64:7].[ext]'
    }
    ]
  },
  plugins: [
  //切换运行环境至生产模式
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      // mangle: {
      //     except: ['$super', '$', 'exports', 'require', 'import', 'from']
      // },
      compress: {
        warnings: false
      }
    }),
    new ExtractTextPlugin("[name].css"),
    new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.bundle.js', Infinity),
    new HtmlWebpackPlugin({
      title: 'yyweb',
      filename: 'index.html',
      template: 'webpack-res/production-index.html',
      isWeixin: false
    })
  ],
  resolve: {
    root: [process.cwd() + '/node_modules', path.resolve('src')],
    extensions: ['', '.js', '.css', '.scss', '.vue', '.png', '.jpg'],
    alias: {
      'vue': resolveNodePath('vue'),
      'vue-router': resolveNodePath('vue-router')
    }
  }

};