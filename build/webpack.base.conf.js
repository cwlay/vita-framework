var path = require('path')
var projectRoot = path.resolve(__dirname, '../')
var autoprefixer = require('autoprefixer')
var precss = require('precss')

module.exports = {
  entry: {
    app: './src/entry.js'
  },
  output: {
    path: path.resolve(__dirname, '../dist/static'),
    publicPath: '/static/',
    filename: '[name].js'
  },
  resolve: {
    extensions: ['', '.js', '.css', '.scss', '.vue', '.png', '.jpg'],
    fallback: [path.join(__dirname, '../node_modules')],
    alias: {
      'src': path.resolve(__dirname, '../src')
    }
  },
  resolveLoader: {
    fallback: [path.join(__dirname, '../node_modules')]
  },
  module: {
    // preLoaders: [
    //   {
    //     test: /\.vue$/,
    //     loader: 'eslint',
    //     include: projectRoot,
    //     exclude: /node_modules/
    //   },
    //   {
    //     test: /\.js$/,
    //     loader: 'eslint',
    //     include: projectRoot,
    //     exclude: /node_modules/
    //   }
    // ],
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue'
      },
      { 
        test: /\.(scss|css)$/, 
        loaders: ['style', 'css', 'postcss', 'sass']
      }, 
      {
        test: /\.js$/,
        loader: 'babel',
        // include: projectRoot,
        query: {
          presets: ['es2015'],
          plugins: ['transform-runtime']
        },
        exclude: /(node_modules|bower_components)/,
      },
      {
        test: /\.json$/,
        loader: 'json'
      },
      {
        test: /\.html$/,
        loader: 'vue-html'
      },
      {
        test: /\.(png|jpg|gif|svg|woff2?|eot|ttf)(\?.*)?$/,
        loader: 'url',
        query: {
          limit: 10000,
          name: '[name].[ext]?[hash:7]'
        }
      }
    ]
  },
  babel: {
    presets: ['es2015', 'stage-0'],
    plugins: ['transform-runtime']
  },
  postcss: function() {
    return [autoprefixer, precss]
  }
  // eslint: {
  //   formatter: require('eslint-friendly-formatter')
  // }
}
