var webpack = require('webpack')
var path = require('path')
var vue = require('vue-loader')
var autoprefixer = require('autoprefixer')
var precss = require('precss')

var resolveBowerPath = function(pluginName) {
    return path.join(__dirname, 'bower_components', pluginName, 'dist', pluginName, '.js')
}

var ExtractTextPlugin = require("extract-text-webpack-plugin")

module.exports = {

  // devtool: '#eval-source-map', 
  devtool: 'source-map',

  entry: {
    entry: ['./dev-client', './src/entry'],
    // vendor: ['./src/vendor/zepto/zepto', './src/vendor/underscore/underscore'],
  },
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, './build/'),
    publicPath: '/static/'
  },
  module: {
    loaders: [{ 
      test: /\.scss$/, 
      loaders: ['style', 'css', 'postcss', 'sass']
    }, 
    {
      test: /\.vue$/, 
      loader: 'vue'
    }, 
    {
      test: /\.css$/,
      loaders: ['style', 'css']
    },
    {
      test: /\.js$/,
      exclude: /(node_modules|bower_components)/,
      loader: 'babel',
      query: {
        presets: ['es2015'],
        plugins: ['transform-runtime']
      }
    },
     {
      test: /\.(png|jpg)\?__inline$/,
      loader: 'url?limit=25000'
    }, {
      test: /\.(png|jpg)$/,
      loader: 'file'
    }
    ]
  },
  plugins: [
    // new webpack.ProvidePlugin({$: 'jQu'}),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    // new webpack.optimize.UglifyJsPlugin({
    //     compress: {
    //         warnings: false
    //     }
    // })
    new ExtractTextPlugin("[name].css")
    // new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.bundle.js', Infinity)
  ],
  resolve: {
    root: [process.cwd() + '/node_modules', path.resolve('src')],
    extensions: ['', '.js', '.css', '.scss', '.vue', '.png', '.jpg'],
    alias: {
      // jQuery: 'zepto/zepto.js'
    }
  },

  babel: {
    presets: ['es2015', 'stage-0'],
    plugins: ['transform-runtime']
  },

  postcss: function() {
    return [autoprefixer, precss]
  },

  //dev-serve
  devServer: {
    contentBase: "./src",
    publicPath: '/static/',
    noInfo: true, //  --no-info option
    hot: true,
    inline: true,
    colors: true,
    // host: '0.0.0.0',
    port: 7788,
    historyApiFallback: true
  }


};