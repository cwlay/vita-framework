import Button from './button.vue'
import Image from './image.vue'
import SlideTabs from './slide-tabs.vue'
import HeaderBar from './header.vue'
import VitaTabs from './tabs.vue'
import VitaTab from './tab.vue'
import VitaContent from './content.vue'
import VitaList from './list.vue'
import VitaItem from './item.vue'

export {Button, Image, SlideTabs, HeaderBar, VitaTabs, VitaTab, VitaContent, VitaList, VitaItem}