export default {

  created() {
    this.uuid = Math.random().toString(36).substring(3,9)
  },

  data() {
    return {
      /*如果值校验不通过(invalid == true)，则返回错误信息*/
      errors: Object,
      /*如果值通过校验则为true*/
      valid: true,
      touched: false,
      pristine: true,
      uuid: Number
    }
  },

  methods: {

  },

  computed: {
      invalid() {
        return !this.valid
      },
      dirty() {
        return !this.pristine
      },
      untouched() {
        return !this.touched
      }
      
  },

  watch: {
    value(newVal) {
      if (this.pristine) {
        this.pristine = false
      }

      this.$dispatch('change', newVal)

    }
  }

}