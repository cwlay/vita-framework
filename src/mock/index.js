import Mock from 'mockjs'

Mock.mock('https://www.oschina.net/action/openapi/news_list', {
  'newslist|20': [{
    'id|+1': 1,
    'title': '@ctitle(1, 10)',
    'author': '@cname',
    'type': 4,
    'authorid|1000-9999': 1,
    'pubDate': '@datetime("yyyy-MM-dd HH:mm:ss")',
    'commentCount|1-50': 1
  }]
})

export default {}