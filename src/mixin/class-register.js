var ClassRegister = {
  methods: {
    _registerClassName(compClassName, className) {
      let childs = this.$parent.$children
      childs.forEach(function(vueComponent) {
        if (vueComponent.$el.className.indexOf(compClassName) != -1) {
          vueComponent.$el.classList.add(className)
        }
      })
    }
  }
};

export default ClassRegister;