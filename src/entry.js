var Vue = require('vue');
var Main = require('./com.vita.main/main.vue'); 
var VueRouter = require('vue-router');
// var vueTouch = require('vue-touch')
var Router = require('./router');
import { Config as StoreConfig} from './store'
import Mock from './mock'

//默认样式
// var Styles = require('./scss/main.scss');
var VitaStyle = require('./scss/vita/vita.scss')
// Vue.use(vueTouch)
Vue.config.debug = true
Vue.use(VueRouter);
StoreConfig(Vue);

//注册路由
var router = new VueRouter({
    history: false,
    saveScrollPosition: false
});


Router(router, Main);

//启动 APP
var App = Vue.extend({
  data() {
    return {
      username: 'chenwenliang'
    }
  }
});
router.start(App, '.vita-app');

window.router = router;