module.exports = function(router, startModule) {

  // function asyncRequire(path) {
  //   return function(resolve) {
  //     // console.log(resolve)
  //     require([path], resolve);
  //   }
  // }

  router.map({

    '/': {
      component: startModule
    },

    '/demo': {
      component: require('./com.vita.main/demo.vue')
    }

    // '/b': {
    //   component: (resolve) => require(['com.yy.bb/index.vue'], resolve)
    // }
  });
}